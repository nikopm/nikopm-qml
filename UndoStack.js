var stack = []
var stackPos = 0

function verify(actions) {
    for(var i = 0; i < actions.length; i++) {
        var action = actions[i]
        if(!action.iid || !action.field || !action.oldValue || !action.newValue) {
            return false
        }
    }
    return true
}

function push(actions) {
    if(!Array.isArray(actions)) {
        actions = [actions]
    }

    if(!verify(actions)) {
        return false
    }

    // Get rid of unneeded items
    if(stackPos < stack.length) {
        stack.splice(stackPos, stack.length - stackPos)
    }

    stackPos = stack.push(actions)

    return actions
}

function undo() {
    if(stackPos < 1) return false

    var item = stack[stackPos - 1]
    if(stackPos > 0) stackPos--
    return item
}

function redo() {
    if(stackPos >= stack.length) return false

    var item = stack[stackPos]
    if(stackPos < stack.length) stackPos++
    return item
}
