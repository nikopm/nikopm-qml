import QtQuick 2.9
import QtQuick.LocalStorage 2.0

import "UndoStack.js" as Undo
import "Storage.js" as Storage

ListModel {
    property int firstDay: -1

    Component.onCompleted: {
        Storage.init()
        getAll()
    }

    function update(item) {
        item.baseColor = "red"
        append(item)
        Storage.add(item)

        if(firstDay < 0 || item.start < firstDay) {
            firstDay = item.start
        }
    }

    function getAll() {
        clear()
        firstDay = -1
        var db = Storage.getHandle()
        try {
            db.transaction(function (tx) {
                var colors = ["red", 'green', 'blue', 'purple', 'gray', 'brown']

                var res = tx.executeSql("SELECT * FROM roadmap_items")
                for(var i = 0; i < res.rows.length; i++) {
                    append({
                       "start": res.rows.item(i).start,
                       "end": res.rows.item(i).end,
                       "title": res.rows.item(i).title,
                       "baseColor": colors[i % colors.length],
                       "iid": res.rows.item(i).iid
                   })
                   if(firstDay > 0 || res.rows.item(i).start < firstDay) {
                       firstDay = res.rows.item(i).start
                   }
                }
            })
        } catch (err) {
            console.log("DATABASE ERROR! " + err)
        }
    }

    function action(action) {
        if(action = Undo.push(action)) {
            action_internal(action, "newValue")
        }
    }

    function action_internal(action, field) {
        for(var i = 0; i < count; i++) {
            var item = get(i)
            for(var j = 0; j < action.length; j++) {
                if(item.iid == action[j].iid) {
                    setProperty(i, action[j].field, action[j][field])
                }
            }
        }
        Storage.update(action, field)
    }

    function undo() {
        var item = Undo.undo()

        if(item) {
            action_internal(item, "oldValue")
        }
    }

    function redo() {
        var item = Undo.redo()
        if(item) {
            action_internal(item, "newValue")
        }
    }
}
