import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

ApplicationWindow {
    id: yapma
    visible: true
    width: 640
    height: 480
    title: qsTr("Yapma")

    property double scale: horizontalScale.value
    property int unitHeight: verticalScale.value
    property date startDate: '2016-01-01'
    property date endDate
    property int firstDay: itemListModel.firstDay

    property var selectedItems: []

    onScaleChanged: itemGrid.performLayout()

    RoadmapItemModel {
        id: itemListModel
    }

    header: Row {
        id: hdr
        Button {
            id: refreshButton
            text: "Refresh"
            onClicked: {
                busy.running = true

                gitlabCall('https://gitlab.com/api/v4/version', function (o) {
                    var earliest = new Date()
                    var latest = new Date()
                    for(var i = 0; i < o.length; i++) {
                        var milestone = o[i]

                        var start = new Date(milestone.start_date?milestone.start_date:'2017-01-01')
                        var end = new Date(milestone.due_date?milestone.due_date:'2017-01-02')

                        itemListModel.update({
                                "title": milestone.title,
                                "start": calculateDayNumber(start),
                                "end": calculateDayNumber(end),
                                "iid": milestone.id
                        })
                    }

                    //itemGrid.performLayout()
                    busy.running = false
                })

            }
        }

        Slider {
            id: horizontalScale
            from: 1
            value: 20
            to: 100
        }

        Slider {
            id: verticalScale
            from: 20
            value: 60
            to: 200
        }

        Button {
            id: undoButton
            text: "Undo"
            onClicked: {
                itemListModel.undo()
            }
        }
        Button {
            id: redoButton
            text: "Redo"
            onClicked: {
                itemListModel.redo()
            }
        }
    }
/*
    MouseArea {
        anchors.fill: parent
        onClicked: {
            Qt.createComponent("Epic.qml").createObject(itemGrid, {
                    "start_date": new Date(),
                    "due_date": new Date() + 5 * 24 * 60 * 60 * 1000
            })
        }
    }
*/

    Flickable {
        anchors {
            left: parent.left
            right: parent.right
            top: hdr.bottom
        }

        contentWidth: itemGrid.width
        height: itemGrid.height
        SwimlaneLayout {
            id: itemGrid
            Repeater {
                id: itemRepeater
                model: itemListModel
                delegate: RoadmapItem{}
            }
        }
    }

    BusyIndicator {
        id: busy
        running: false
        anchors.centerIn: parent
    }

    function gitlabCall(url, callback) {
        var req = new XMLHttpRequest;
//        req.open("GET", "https://gitlab.com/api/v4/projects/1641259/milestones")
        req.open("GET", "testdata.json")
        req.onreadystatechange = function() {
            var status = req.readyState
            if (status === XMLHttpRequest.DONE) {
                var result = JSON.parse(req.responseText)
                callback(result)
            }
        }
        req.send();
    }

    function calculateDayNumber(daysTo) {
        return daysTo.getTime() / (24 * 60 * 60 * 1000)
    }

    function selected(iid, multiselect) {
        console.log("Selecting " + iid + ", " + (multiselect?"multi":"not multi"))
        var index = selectedItems.indexOf(iid)
        if(multiselect) {
            if(index == -1) {
                selectedItems = selectedItems.concat([iid])
            } else {
                selectedItems.splice(index, 1)
                selectedItemsChanged()
            }
        } else {
            if(index == -1) {
                selectedItems = [iid]
            } else {
                selectedItems = []
            }
        }
    }

    function action(action) {
        itemListModel.action(action)
    }
}
