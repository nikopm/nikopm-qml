var db

function getHandle() {
    return LocalStorage.openDatabaseSync("YapmaDB", "1", "Roadmap Database", 1000000)
}

function init() {
    var db = getHandle()
    try {
        db.transaction(function (tx) {
            tx.executeSql('CREATE TABLE IF NOT EXISTS roadmap_items (iid text,start int,end int,title text)')
        })
    } catch (err) {
        console.log("Error creating table in database: " + err)
    };
}

function update(items, field) {
    var db = getHandle()
    try {
        db.transaction(function (tx) {
            for(var i = 0; i < items.length; i++) {
                tx.executeSql('UPDATE roadmap_items SET ' + items[i].field + '=' + items[i][field] + " WHERE iid=" + items[i].iid)
            }
        })
    } catch (err) {
        console.log("Error updating items! " + err)
    }
}

function add(item) {
    var db = getHandle()
    try {
        db.transaction(function (tx) {
            tx.executeSql("INSERT INTO roadmap_items (iid,start,end,title) VALUES ("+item.iid+","+item.start+","+item.end+",'"+item.title+"')")
        })
    } catch (err) {
        console.log("Error updating items! " + err)
    }
}

