import QtQuick 2.9

Item {
    id: layout

    property bool ready: false

    property int swimLaneHeight: (yapma.unitHeight * 1.1)

    property int swimLanes: 1

    property int firstDay: yapma.firstDay

    height: swimLanes * swimLaneHeight
    width: childrenRect.width

    onChildrenChanged: performLayout()
//    onWidthChanged: performLayout()
//    onHeightChanged: performLayout()
//    onChildrenRectChanged: performLayout()
    onFirstDayChanged: performLayout()

    Rectangle {
        anchors.fill: parent
        color: "lightsteelblue"
    }
    function layoutItem(obj) {
        if(obj.from === undefined || obj.days < 0) {
            obj.visible = false
            return
        }

        var swimLane = 0
        var maxSwimLane = 0

        for(var idx = 0; idx < children.length; idx++) {
            var i = children[idx]
            if(i == obj || i.from === undefined) continue

            if(i.from < obj.to && i.to > obj.from && i.swimLane == swimLane) {
                //console.log("i.from: " + i.from + ", obj.to: " + obj.to + ", i.to: " + i.to + ", obj.from: " + obj.from)

                swimLane = i.swimLane + 1
                idx = 0
            }

            if(i.swimLane > maxSwimLane) maxSwimLane = i.swimLane
        }

        obj.swimLane = swimLane
        if(swimLane > maxSwimLane) maxSwimLane = swimLane

        swimLanes = maxSwimLane + 1

        obj.x = (obj.from - firstDay) * yapma.scale
        obj.y = obj.swimLane * swimLaneHeight
        obj.width = (obj.to - obj.from) * yapma.scale
    }

    function performLayout() {
        var maxX = 0
        var maxSwimlane = 0
        for (var i = 0; i < children.length; ++i) {
            var obj = children[i]

            if(obj.days === undefined) continue

            layoutItem(obj)
        }

        swimLanes = maxSwimlane + 1
    }

    function mapDateToX(d) {
        return yapma.scale * calculateDayNumber(d)
    }

}
