import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    id: item
    property int days: end - start
    width: yapma.scale * days
    height: yapma.unitHeight
    property int from: start
    property int to: end
    property int swimLane: 0
    property bool isUnSelected: yapma.selectedItems.count > 0 && yapma.selectedItems.indexOf(iid) == -1

    onFromChanged: itemGrid.layoutItem(this)
    onToChanged: itemGrid.layoutItem(this)

    states: [
        State {
            name: "unselected"
            when: yapma.selectedItems.indexOf(iid) == -1 && yapma.selectedItems.length > 0
            PropertyChanges {
                target: gradientStart
                color: "#55555555"
            }
            PropertyChanges {
                target: gradientEnd
                color: "#66666666"
            }
            PropertyChanges {
                target: rectBorder
                color: "#77777777"
            }
        },
        State {
            name: "Dragging"
            when: mouseArea.pressed
            PropertyChanges {
                target: rect
                rotation: 3
            }
            PropertyChanges {
                target: item
                z: 100
            }
        }
    ]
    transitions: [
        Transition {
            ColorAnimation {
                duration: 200
            }
        }
    ]
    Behavior on y {
        NumberAnimation { easing.type: Easing.OutCurve; easing.amplitude: 1.0; easing.period: 1.0; duration: 100 }
    }

    Rectangle {
        id:rect
        anchors.fill: parent

        radius: Math.min(parent.height / 2, parent.width / 4)
        gradient: Gradient {
            GradientStop {
                id: gradientStart
                position: 1.0
                color: Qt.darker( baseColor, 1.2)
            }
            GradientStop {
                id: gradientEnd
                position: 0.0
                color: baseColor
            }
        }
        border {
            id:rectBorder
            width: 2
            color: Qt.darker(baseColor, 1.3)
        }

        Label {
            id:titleLabel
            text: title
            color: "#DDDDDD"
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.Wrap
        }

        Behavior on rotation {
            NumberAnimation { easing.type: Easing.OutCurve; easing.amplitude: 1.0; easing.period: 1.0; duration: 50 }
        }
    }
    MouseArea {
        id: mouseArea

        cursorShape: Qt.OpenHandCursor
        anchors.fill: parent
        preventStealing: true
        property int startPos: 0
        property bool moved: false
        property int originalStartDay: 0
        onPressed: {
            moved = false
            startPos = mouseX
            originalStartDay = start
        }
        onMouseXChanged: {
            var s = Math.round((mouseX - startPos) / yapma.scale)

            if(!moved && Math.abs(s) >= 1) {
                moved = true
            }

            start += s
            end += s
        }
        onClicked: {
            if(!moved) {
                yapma.selected(iid, mouse.modifiers & Qt.ShiftModifier)
            }
        }
        onPressAndHold: {}
        onReleased: {
            if(moved) {
                var diff = start - originalStartDay
                var originalEndDay = end - diff

                yapma.action([
                    {iid: iid, field: "start", oldValue: originalStartDay, newValue: start},
                    {iid: iid, field: "end", oldValue: originalEndDay, newValue: end}
                ])
            }
       }
    }

    MouseArea {
        id: endDrag
        cursorShape: Qt.SplitHCursor
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        width: 10
        preventStealing: true
        hoverEnabled: false
        property int originalEndDay: 0
        onPressed: originalEndDay = end
        onMouseXChanged: {
            var e = end + Math.round(mouseX / yapma.scale)
            if(e - start <= 0) {
                end = start + 1
            } else {
                end = e
            }
        }

        onReleased: {
            yapma.action({iid: iid, field: "end", oldValue: originalEndDay, newValue: end})
        }
    }

    MouseArea {
        id: startDrag
        cursorShape: Qt.SplitHCursor
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        width: 10
        preventStealing: true
        onPressed: originalEndDay = end
        onMouseXChanged: {
            var s = start + Math.round(mouseX / yapma.scale)
            if(end - s <= 0) {
                start = end - 1
            } else {
                start = s
            }
        }

        onReleased: {
            yapma.action({iid: iid, field: "start", oldValue: originalStartDay, newValue: start})
        }
    }
}
